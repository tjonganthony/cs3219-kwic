import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by xinke
 */
public class CircularShiftTest {

    private LineStorage originalLineStorage;
    private LineStorage expectedShiftedLines;
    private LineStorage shiftedLines;
    private List<String> lines;
    private CircularShift shifter;
    private List<String> ignoredWords;

    @Before
    public void setUp() throws Exception {
        expectedShiftedLines = new LineStorage();
        originalLineStorage = new LineStorage();
        shiftedLines = new LineStorage();
        lines = new ArrayList<String>();
        makeTestLinesList();
        shifter = new CircularShift(shiftedLines);
        ignoredWords = new ArrayList<String>();
    }

    private void makeTestLinesList() {
        lines.add("The Day after Tomorrow");
        lines.add("Fast and Furious");
        lines.add("Man of Steel");
    }

    @Test
    public void testShiftOneWordInOneLine(){
        addOneLineToOriginalStorage(lines.get(0));
        String expected = "Tomorrow The Day after";
        expectedShiftedLines.addLine(expected);
        shifter.shiftWords(originalLineStorage,0);
        assertEquals(expectedShiftedLines.getLineAtIndex(0),shifter.getShiftedLineAt(0));

    }

    @Test
    public void testShiftAllWordsInALine(){
        addOneLineToOriginalStorage(lines.get(0));
        expectedShiftedLines.addLine("Tomorrow The Day after");
        expectedShiftedLines.addLine("after Tomorrow The Day");
        expectedShiftedLines.addLine("Day after Tomorrow The");
        expectedShiftedLines.addLine("The Day after Tomorrow");
        shifter.shiftWords(originalLineStorage, 0);
        for (int i = 0; i < expectedShiftedLines.getlineCount(); i++){
            assertEquals(expectedShiftedLines.getLineAtIndex(i),shifter.getShiftedLineAt(i));
            expectedShiftedLines.getLineAtIndex(i);
        }
    }

    private void addOneLineToOriginalStorage(String aLine) {
        originalLineStorage.addLine(aLine);
    }

    @Test
    public void testShiftOneWordInLines(){
        addAllTestLinesToOriginalStorage();
        String expected1 = "Steel Man of";
        String expected2 = "of Steel Man";
        String expected3 = "Man of Steel";
        expectedShiftedLines.addLine(expected1);
        expectedShiftedLines.addLine(expected2);
        expectedShiftedLines.addLine(expected3);
        shifter.shiftWords(originalLineStorage,2);
        for (int i = 0; i < expectedShiftedLines.getlineCount(); i++) {
            assertEquals(expectedShiftedLines.getLineAtIndex(i), shifter.getShiftedLineAt(i));
        }
    }

    @Test
    public void testFilterIgnoredWords(){
        makeIgnoreWordsList();
        addOneLineToOriginalStorage(lines.get(0));
        shifter.shiftWords(originalLineStorage,0);
        shifter.filterIgnoredWords(ignoredWords);
        expectedShiftedLines.addLine("Tomorrow The Day after");
        expectedShiftedLines.addLine("Day after Tomorrow The");
        assertEquals(expectedShiftedLines.getLineAtIndex(0),shifter.getShiftedLineAt(0));
        assertEquals(expectedShiftedLines.getLineAtIndex(1), shifter.getShiftedLineAt(1));
    }

    private void makeIgnoreWordsList() {
        ignoredWords.add("is");
        ignoredWords.add("the");
        ignoredWords.add("of");
        ignoredWords.add("and");
        ignoredWords.add("as");
        ignoredWords.add("a");
        ignoredWords.add("after");
    }

    private void addAllTestLinesToOriginalStorage() {
        for (String line: lines){
            addOneLineToOriginalStorage(line);
        }
    }

}