import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InputTest {

    private Input input;
    private String inputLine;
    private List<String> inputLines;
    private LineStorage lineStorage;


    @Before
    public void setUp() throws Exception {
        input = new Input();
        inputLine = "The Day after Tomorrow";
        inputLines = makeTestStrings();
        lineStorage = new LineStorage();
    }

    @Test
    public void testParseOneLineToString() throws Exception{
        assertEquals(inputLine,input.parseStreamOfOneLine(makeOneLineInputStreamBuffer()));
    }

    @Test
    public void testParseMultipleLinesToString() throws Exception{
        try( BufferedReader reader = makeMultilineInputStream()) {
            assertEquals(inputLines, input.parseStreamOfMultipleLines(reader));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddingOneLineToLineStorageAfterParsing() throws IOException {
        input.parseLines(lineStorage,makeOneLineInputStreamBuffer());
        assertEquals(inputLine, lineStorage.getLineAtIndex(0));
    }

    @Test
    public void testAddMultipleLinesToLineStorageAfterParsing() throws IOException {
        input.parseLines(lineStorage,makeMultilineInputStream());
        int counter = 0;
        for(String line: inputLines){
            assertEquals(line, lineStorage.getLineAtIndex(counter));
            counter++;
        }
    }

    private List<String> makeTestStrings() {
        List<String> multiInputLines = new ArrayList<String>();
        multiInputLines.add("The Day after Tomorrow");
        multiInputLines.add("Fast and Furious");
        multiInputLines.add("Man of Steel");
        return multiInputLines;
    }

    private  BufferedReader makeMultilineInputStream() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (String line: inputLines){
            line+="\r\n";
            baos.write(line.getBytes());
        }
        InputStream in = new ByteArrayInputStream(baos.toByteArray());
        return new BufferedReader(new InputStreamReader(in));
    }

    private BufferedReader makeOneLineInputStreamBuffer() {
        InputStream inputStream = new ByteArrayInputStream(inputLine.getBytes(StandardCharsets.UTF_8));
        return new BufferedReader(new InputStreamReader(inputStream));
    }
}
