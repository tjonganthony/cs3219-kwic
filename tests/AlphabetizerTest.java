import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by xinke
 */
public class AlphabetizerTest {

    private Alphabetizer alphabetizer;
    private String testLineA;
    private String testLineB;
    private String testLineC;

    @Before
    public void setUp() throws Exception {
        alphabetizer = new Alphabetizer();
        testLineA = "A line of string";
        testLineB = "B line of string";
        testLineC = "C line of string";
    }

    @Test
    public void testSortAListOfLinesByLetter(){
        LineStorage lines = new LineStorage();
        lines.addLine(testLineC);
        lines.addLine(testLineB);
        lines.addLine(testLineA);
        alphabetizer.sortLines(lines);
        assertEquals(lines.getLineAtIndex(0),testLineA);
        assertEquals(lines.getLineAtIndex(1),testLineB);
        assertEquals(lines.getLineAtIndex(2),testLineC);
    }

}
