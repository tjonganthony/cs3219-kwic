import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by xinke on 30/1/2017.
 */
public class LineStorageTest {

    private String aLine;
    private LineStorage lineStorage;

    @Before
    public void setUp() throws Exception {
        aLine = "The Day after Tomorrow";
        lineStorage = new LineStorage();
    }

    @Test
    public void testAddOneLineToLineStorage(){
        lineStorage.addLineAndNotify(aLine);
        assertEquals(aLine, lineStorage.getLineAtIndex(0));
    }
}