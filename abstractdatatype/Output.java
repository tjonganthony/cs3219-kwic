package abstractdatatype;

interface Output {
    void display(AlphabetShifter shifter);
}
