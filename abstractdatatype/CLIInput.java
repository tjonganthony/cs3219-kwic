package abstractdatatype;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author tjonganthony
 * Implementation of Input using Command Line interface
 */
final class CLIInput implements Input{
    private Scanner sc = new Scanner(System.in);
    
    public CLIInput() {
    }

    @Override
    public List<Character> readTitles() {
        List<Character> titles = new ArrayList<>();
        
        System.out.println("How many titles do you want to add?");
        int n = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < n; i++) {
            titles.add(new Title(sc.nextLine()));
        }
        
        return titles;
    }

    @Override
    public Character readWordsToIgnore() {
        List<String> ignoredWords = new ArrayList<String>();

        System.out.println("How many blocked words do you want to add?");
        int n = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < n; i++) {
            ignoredWords.add(sc.next());
        }
        
        return new IgnoredWords(ignoredWords);
    }

}
