package abstractdatatype;

import java.util.List;

interface Input {
    public List<Character> readTitles();
    public Character readWordsToIgnore();
}
