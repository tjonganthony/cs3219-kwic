package abstractdatatype;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Input input = new CLIInput();
        List<Character> titles= input.readTitles();
        Character ignoredWords = input.readWordsToIgnore();
        
        WordDetector detector = new WordDetector(ignoredWords);
        List<String> results = new ArrayList<>();
        CircularShifter circularShifter = new CircularShifter(null, ignoredWords, " ");
        for (Character title : titles) {
            circularShifter.setTitle(title);
            results.addAll(circularShifter.getAllShiftedTitles());
        }
        
        AlphabetShifter shifter = new AlphabetSorter(results);
        Output output = new CLIOutput();
        output.display(shifter);
    }
}
