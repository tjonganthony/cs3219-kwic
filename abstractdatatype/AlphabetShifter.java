package abstractdatatype;

interface AlphabetShifter {
    public String get(int i);
    public int size();
}
