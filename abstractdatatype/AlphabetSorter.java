package abstractdatatype;

import java.util.ArrayList;
import java.util.List;

final class AlphabetSorter implements AlphabetShifter {
    List<String> sorted;
    
    public AlphabetSorter(List<String> sentences) {
        sorted = new ArrayList<>(sentences);
        sorted.sort(null);
    }

    @Override
    public String get(int i) {
        return sorted.get(i);
    }
    
    @Override
    public int size() {
        return sorted.size();
    }
}
