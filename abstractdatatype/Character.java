package abstractdatatype;

import java.util.List;

interface Character {
    public List<String> getWords();
    public int getWordCount();
}
