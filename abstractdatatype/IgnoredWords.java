package abstractdatatype;

import java.util.List;

final class IgnoredWords implements Character {
    private final List<String> words;
    
    public IgnoredWords(List<String> words) {
        this.words = words;
    }

    @Override
    public List<String> getWords() {
        return words;
    }

    @Override
    public int getWordCount() {
        return words.size();
    }

}
