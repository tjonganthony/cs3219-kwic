package abstractdatatype;

import java.util.ArrayList;
import java.util.List;

final class WordDetector {
    private final List<String> ignoredWords;
    
    /**
     * 
     * @param ignoredWords: All words that want to be detected
     */
    public WordDetector(Character ignoredWords) {
        this.ignoredWords = new ArrayList<>();
        for (String word : ignoredWords.getWords()) {
            this.ignoredWords.add(word.toLowerCase());
        }
    }
    
    /**
     * 
     * @param title TODO
     * @param words: the list of words that want to be detected
     * @return List of Integer indicating the index of the ignored words
     */
    public List<Integer> findIgnoredWord(Character title) {
        List<String> words = title.getWords();
        ArrayList<Integer> answers = new ArrayList<>();
        
        // Don't iterate through strings because might contain duplicate words
        for (int i = 0; i < words.size(); i++) {
            if (ignoredWords.contains(words.get(i).toLowerCase())) {
                answers.add(i);
            }
        }
        
        return answers;
    }
}
