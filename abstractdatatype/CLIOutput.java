package abstractdatatype;

final class CLIOutput implements Output {

    public CLIOutput() {}

    @Override
    public void display(AlphabetShifter shifter) {
        for (int i = 0; i < shifter.size(); i++) {
            System.out.println(shifter.get(i));
        }
    }

}
