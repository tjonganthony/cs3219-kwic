package abstractdatatype;

import java.util.Arrays;
import java.util.List;

final class Title implements Character {
    private final List<String> words;
    
    public Title(String title) {
        words = Arrays.asList(title.split(" "));
    }

    @Override
    public List<String> getWords() {
       return words;
    }

    @Override
    public int getWordCount() {
        return words.size();
    }
}
