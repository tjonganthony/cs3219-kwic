package abstractdatatype;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

final class CircularShifter {
    private Character title;
    private String combiner;
    private WordDetector detector;
    
    /**
     * 
     * @param words: the list of word that will be shifted
     * @param combiner: A string that will be a connector between 2 word when shifted and joined
     */
    public CircularShifter(Character title, Character ignoredWords, String combiner) {
        this.title = title;
        this.combiner = combiner;
        detector = new WordDetector(ignoredWords);
    }
    
    public void setCombiner(String combiner) {
        this.combiner = combiner;
    }
                
    public void setTitle(Character title) {
        this.title = title;
    }
    
    public void setIgnoredWords(Character ignoredWords) {
        detector = new WordDetector(ignoredWords);
    }
    
    public List<String> getAllShiftedTitles() {
        List<String> result = new ArrayList<>();
        List<Integer> ignoredPosition = detector.findIgnoredWord(title);
        
        for (int i = title.getWordCount() - 1; i >= 0; i--) {
            Collections.rotate(title.getWords(), 1);
            String test = String.join(combiner, title.getWords());
            if (ignoredPosition.contains(i)) {
                continue;
            }
            result.add(test);
        }
       return result;
    }
}
