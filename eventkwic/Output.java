/**
 * Created by xinke
 */
public class Output {
    public void print(LineStorage shiftedLines){
        System.out.println("The KWICs are :");
        for(String line: shiftedLines.getAllLines()){
            System.out.println(line);
        }
    }
}
