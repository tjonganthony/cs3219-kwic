import java.util.Observable;
import java.util.Observer;

/**
 * Created by xinke
 */
public class Alphabetizer implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        LineStorage lines = (LineStorage) o;
        sortLines(lines);
    }

    public void sortLines(LineStorage lines) {
        lines.sortLines();
    }
}
