import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Input {


    public int readInt(BufferedReader inputReader) {
        try {
            return Integer.parseInt(inputReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String parseStreamOfOneLine(BufferedReader inputReader) throws IOException {
        return inputReader.readLine();
    }

    public List<String> parseStreamOfMultipleLines(BufferedReader multipleLineStream) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = null;
        while(((line = parseStreamOfOneLine(multipleLineStream))!=null)){
           lines.add(line.trim());
        }
        return lines;
    }

    public void parseLines(LineStorage lineStorage, BufferedReader bufferedReader) throws IOException {
        List<String> lines = parseStreamOfMultipleLines(bufferedReader);
        for (String line : lines){
            lineStorage.addLineAndNotify(line);
        }
    }

    public void parseLine(LineStorage lineStorage,BufferedReader bufferedReader) throws Exception {
        String line = parseStreamOfOneLine(bufferedReader);
        lineStorage.addLineAndNotify(line);
    }

    public void parseNoiseWords(LineStorage lineStorage,BufferedReader inputSource) throws IOException {
        String lineOfNoiseWords = parseStreamOfOneLine(inputSource);
        List<String> noiseWords = Arrays.asList(lineOfNoiseWords.split(","));
        for (String s: noiseWords){
            lineStorage.addNoiseWords(s.trim().toLowerCase());
        }
    }
}
