/**
 * Created by xinke
 */
public class LineAddedEvent implements LineStorageChangedEvent{
    private String args;

    public LineAddedEvent(String args){
       this.args=args;
    }
    @Override
    public Enum getType() {
        return EventTypes.ADD;
    }

    @Override
    public String getArgs() {
        return args;
    }
}
