import java.util.*;

public class CircularShift implements Observer {
    private LineStorage shiftedLines;

    public CircularShift(LineStorage shiftedLines){
        this.shiftedLines = shiftedLines;
    }

    @Override
    public void update(Observable observable, Object event) {
        LineStorage originalLineStorage = (LineStorage) observable;
        LineAddedEvent e = (LineAddedEvent) event;
        shiftWords(originalLineStorage,Integer.parseInt(e.getArgs()));
    }

    public void shiftWords(LineStorage lineStorage, int lineIndex) {
        String line = lineStorage.getLineAtIndex(lineIndex);
        List<String> words = splitLineAsWordList(line);
        for (int wordIndex = 0; wordIndex < words.size(); wordIndex++){
            shiftWord(lineStorage,words);
            filterIgnoredWords(lineStorage.getNoiseWords());
            shiftedLines.addLineAndNotify(combineShiftedWords(words));
        }
    }

    private void shiftWord(LineStorage lines, List<String> words) {
        String lastWord = words.get(words.size()-1);
        words.remove(words.size()-1);
        words.add(0, lastWord);
    }

    private String combineShiftedWords(List<String> words) {
        StringBuilder shiftedWords = new StringBuilder();
        for (String word: words){
            shiftedWords.append(word);
            shiftedWords.append(" ");
        }
        return shiftedWords.toString().trim();
    }

    private ArrayList<String> splitLineAsWordList(String line) {
        return new ArrayList<String>(Arrays.asList(line.split("\\s+")));
    }

    public LineStorage getShiftedLines(){
        return shiftedLines;
    }

    public String getShiftedLineAt(int index){
        return shiftedLines.getLineAtIndex(index);
    }

    public void filterIgnoredWords(List<String> ignoredWords) {
        for (int lineIndex =0; lineIndex < shiftedLines.getlineCount(); lineIndex++){
            if (ignoredWords.contains(shiftedLines.getWordAt(lineIndex,0).toLowerCase())) {
                shiftedLines.removeLine(lineIndex);
            }
        }
    }
}
