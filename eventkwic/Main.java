import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    private static final String WELCOME_MESSAGE =
            " .----------------.  .----------------.  .----------------.  .----------------. \n" +
            "| .--------------. || .--------------. || .--------------. || .--------------. |\n" +
            "| |  ___  ____   | || | _____  _____ | || |     _____    | || |     ______   | |\n" +
            "| | |_  ||_  _|  | || ||_   _||_   _|| || |    |_   _|   | || |   .' ___  |  | |\n" +
            "| |   | |_/ /    | || |  | | /\\ | |  | || |      | |     | || |  / .'   \\_|  | |\n" +
            "| |   |  __'.    | || |  | |/  \\| |  | || |      | |     | || |  | |         | |\n" +
            "| |  _| |  \\ \\_  | || |  |   /\\   |  | || |     _| |_    | || |  \\ `.___.'\\  | |\n" +
            "| | |____||____| | || |  |__/  \\__|  | || |    |_____|   | || |   `._____.'  | |\n" +
            "| |              | || |              | || |              | || |              | |\n" +
            "| '--------------' || '--------------' || '--------------' || '--------------' |\n" +
            " '----------------'  '----------------'  '----------------'  '----------------'\n"+
            "> Hi, welcome to KWIC system.";

    public static void main(String[] args) throws Exception {
        LineStorage lines = new LineStorage();
        LineStorage shiftedLines = new LineStorage();

        Input input = new Input();
        CircularShift shifter = new CircularShift(shiftedLines);
        Alphabetizer alphabetizer = new Alphabetizer();
        Output output = new Output();

        lines.addObserver(shifter);
        shiftedLines.addObserver(alphabetizer);

        BufferedReader inputSource = new BufferedReader(new InputStreamReader(System.in));

        System.out.println(WELCOME_MESSAGE);
        printSystemMessage("Please enter list of noise words separated by comma >>");
        input.parseNoiseWords(lines,inputSource);

        printSystemMessage("How many lines do you want to add ? >>");

        int numLines = input.readInt(inputSource);
        printSystemMessage("Please enter each line followed by <Enter> >>");

        for (int i = 0; i < numLines; i++){
            input.parseLine(lines,inputSource);
        }
        output.print(shiftedLines);
    }

    public static void printSystemMessage(String s){
        System.out.println("KWIC: "+s);
    }
}
