/**
 * Created by xinke
 */
public interface LineStorageChangedEvent {
    Enum getType();
    String getArgs();
}
