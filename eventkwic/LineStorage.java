import java.util.*;

/**
 * Created by xinke on 30/1/2017.
 */
public class LineStorage extends Observable {

    private List<String> lines = new ArrayList<String>();
    private List<String> noiseWords = new ArrayList<>();

    public void addLineAndNotify(String aLine) {
        addLine(aLine);
        setChanged();
        notifyObservers(new LineAddedEvent(lines.indexOf(aLine)+""));
    }

    public String getLineAtIndex(int i) {
        return lines.get(i);
    }

    public void addLine(String aLine) {
        lines.add(aLine);
    }


    public void replaceLine(int lineIndex, String newLine) {
        if (!lines.isEmpty()) {
            lines.remove(lineIndex);
        }
        lines.add(lineIndex, newLine);
    }

    public List<String> getAllLines(){
        return lines;
    }

    public void sortLines(){
        Collections.sort(lines);
    }

    public int getlineCount() {
        return lines.size();
    }

    public String getWordAt(int lineIndex, int wordIndex) {
        String line = lines.get(lineIndex);
        ArrayList<String> words = new ArrayList<String>(Arrays.asList(line.split("\\s+")));
        return words.get(wordIndex);
    }

    public void removeLine(int lineIndex) {
        lines.remove(lineIndex);
    }

    public void addNoiseWords(String s) {
        noiseWords.add(s);
    }

    public boolean isNoiseWord(String s){
        for (String noiseWord: noiseWords){
            if (s.toLowerCase().equals(noiseWord.toLowerCase())) return true;
        }
        return false;
    }

    public List<String> getNoiseWords() {
        return noiseWords;
    }
}
